import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    const appName = 'Be Productive!';

    return MaterialApp(
      title: appName,
      theme: ThemeData(
        // Define the default brightness and colors.

        primaryColor: Colors.orangeAccent,

        // Define the default font family.
        fontFamily: 'Lato',
        // Define the default `TextTheme`. Use this to specify the default
        // text styling for headlines, titles, bodies of text, and more.
      ),
      home: const MyHomePage(
        title: 'Be Productive',
      ),
    );
  }
}

class MyHomePage extends StatelessWidget {
  final String title;

  const MyHomePage({Key? key, required this.title}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Be Productive!'),
        backgroundColor: Colors.orangeAccent,
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.orangeAccent,
        onPressed: null,
        child: Text('+'),
      ),
      drawer: Drawer(
        // Add a ListView to the drawer. This ensures the user can scroll
        // through the options in the drawer if there isn't enough vertical
        // space to fit everything.
        child: ListView(
          // Important: Remove any padding from the ListView.
          padding: EdgeInsets.zero,
          children: <Widget>[
            DrawerHeader(
              child: Center(child: Text('Apps')),
              decoration: BoxDecoration(
                color: Colors.orangeAccent,
              ),
            ),
            ListTile(
              title: Text('Home'),
            ),
            ListTile(
              title: Text('User'),
            ),
            ListTile(
              title: Text('To Do List'),
            ),
            ListTile(
              title: Text('Schedule'),
            ),
            ListTile(
              title: Text('Anonymous Messages'),
            ),
            ListTile(
              title: Text('Forum Discussion'),
            ),
            ListTile(
              title: Text('Note'),
            ),
            ListTile(
              title: Text('News'),
            ),
            ListTile(
              title: Text('Repo Github'),
            ),
          ],
        ),
      ),
      bottomNavigationBar: BottomAppBar(
        child: Image.asset('images/bottom.png'),
      ),
    );
  }
}

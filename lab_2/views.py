from lab_2.models import Note
from django.shortcuts import render
from django.http.response import HttpResponse
from django.core import serializers

# Create your views here.
judul = 'Short Message'  # TODO Implement this
penerima = "You"
pengirim = "Me"  # TODO Implement this, format (Year, Month, Date)
message = "Wish u all the best"  # TODO Implement this


def index(request):
    response = {'judul':judul,
                'penerima': penerima,
                'message' :message,
                'pengirim': pengirim
                }
    return render(request, 'lab2.html', response)

def xml(request):
    data = serializers.serialize('xml', Note.objects.all())
    return HttpResponse(data, content_type="application/xml")

def json(request):
    
    data = serializers.serialize('json', Note.objects.all())
    return HttpResponse(data, content_type="application/json")



from django.shortcuts import render
from datetime import datetime, date
from lab_1.models import Friend
from lab_1.views import calculate_age
from lab_3.forms import FriendForm
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect


mhs_name = 'Reyza Abyan Gerrit Caloh'  # TODO Implement this
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(2003,1, 24)  # TODO Implement this, format (Year, Month, Date)
npm = 2006463534  # TODO Implement this

@login_required(login_url="/admin/login/")
def index(request):
    friends = Friend.objects.all() # TODO Implement this
    response = {'friends': friends}
    return render(request, 'lab3_index.html', response)


def add_friend(request):
    if request.method == 'POST':
        form = FriendForm(request.POST)
        if form.is_valid():
            form.save()  
            return HttpResponseRedirect('/lab-3/')  # Redirect on finish
    else:
        form = FriendForm()

    return render(request, 'lab3_form.html', {'form': form})


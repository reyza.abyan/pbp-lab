
1. Apakah perbedaan antara JSON dan XML?

- JSON berasal dari bahasa JavaScript, sedangkan XML berasal dari SGML(Standard Generalized Markup Language)
- JSON mendukung penggunaan array sedangkan, XML tidak mendukung penggunaan array
- JSON tidak mendukung implementasi komentar, sedangkan XML mendukung implementasi komentar
- JSON hanya mendukung encoding UTF-8, sedangkang XML mampu mendukung berbagai encoding

2. Apakah perbedaan antara HTML dan XML?

- HTML bersifat statik, sedangkan XML bersifat dinamik
- HTML digunakan untuk menampilkan data, sedangkan XML digunakan untuk menyimpan data
- HTML tidak bersifat Case sensitive, sedangkan XML bersifat Case sensitive
- HTML mampu mengabaikan error kecil, sedangkan pada XML tidak boleh ada error sama sekali

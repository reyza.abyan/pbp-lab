from django.shortcuts import render, redirect
from lab_4.forms import NoteForm
from django.http import HttpResponseRedirect
from .models import Note

# Create your views here.

judul = 'Short Message'  # TODO Implement this
penerima = "You"
pengirim = "Me"  # TODO Implement this, format (Year, Month, Date)
message = "Wish u all the best"  # TODO Implement this


def index(request):
    notes = Note.objects.all()
    data = {"list_note" : notes} 
    return render(request, 'lab4_bsindex.html', context=data)

def add_message(request):
    if request.method == 'POST':
        form = NoteForm(request.POST)
        if form.is_valid():
            form.save()  
            Note.objects.create(
                form
            )
            return redirect('lab-4:index')  # Redirect on finish
    else:
        form = NoteForm()

    return render(request, 'lab4_bsindex.html', {'form': form})


def card_message(request):
    notes = Note.objects.all()
    data = {"list_note" : notes} 
    return render(request, 'lab4_cdindex.html', context=data)



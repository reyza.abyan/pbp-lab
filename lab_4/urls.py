from django.urls import path
from .views import index, add_message,card_message


app_name = 'lab4'


urlpatterns = [
    path('', index, name='index'),
    path('add_message/', add_message, name="add_message"),
    path('card_message/', card_message, name="card_message")
    
   
    # TODO Add friends path using friend_list Views
]

from django.db import models

# Create your models here.

class Note(models.Model):
    title = models.CharField(max_length=50)
    penerima = models.CharField(max_length=50)
    pengirim = models.CharField(max_length=50)
    message = models.TextField()
